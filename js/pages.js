function redirectTo(page) {
    $('#iframe').attr('src', page + '.html');
    $('#titlePage').text(page);
}

function parentRedirectTo(page) {
    window.parent.$('#iframe').attr('src', page + '.html');
}