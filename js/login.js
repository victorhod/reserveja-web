
function doLogin() {
    const data = {
        email: $('#email').val(),
        password: $('#senha').val()
    };

    $.ajax({
        method: "POST",
        url: "https://api-reserveja-prod.herokuapp.com/establishments/login",
        data: JSON.stringify(data),
        contentType: "application/json"
    })
    .done(function(response, textStatus, XMLHttpRequest) {     
        localStorage.setItem('oauth', XMLHttpRequest.getResponseHeader('x-auth'));   
        window.location.href = "/home";
    })
    .fail(function(jqXHRm, textStatus, msg) {
        alert(jqXHRm.responseJSON.message);
    });
}

function doLogout() {
    $.ajax({        
        url: "https://api-reserveja-prod.herokuapp.com/establishments/me/token",
        type: 'DELETE',
        headers: {
            "X-Auth": localStorage.getItem("oauth")
        },
        contentType: "application/json"
    })
    .done(function(response, textStatus, XMLHttpRequest) {        
        localStorage.clear();
        window.location.href = "/";
    })
    .fail(function(jqXHRm, textStatus, msg) {
        alert(jqXHRm.textStatus);
    });
}