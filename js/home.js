function loadUser() {
    $.ajax({
        method: "GET",
        url: 'https://api-reserveja-prod.herokuapp.com/establishments/me',
        headers: {
            "X-Auth": localStorage.getItem("oauth")
        },
        contentType: "application/json"
    })
        .done(function (response) {
            localStorage.setItem('_id', response._id );
            var template = $('#user_template').html();
            var rendered = Mustache.render(template, { name: response.name });
            $('#user_name').append(rendered);
        })
        .fail(function (jqXHRm, textStatus, msg) {
            alert(jqXHRm.responseJSON.message);
        });
}