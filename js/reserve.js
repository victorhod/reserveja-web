function loadReserves() {
    $.ajax({
        method: "GET",
        url: 'https://api-reserveja-prod.herokuapp.com/reserve/e' + localStorage.getItem('_id'),
        headers: {
            "X-Auth": localStorage.getItem("oauth")
        },
        contentType: "application/json"
    })
        .done(function (response) {
            $('#loader').remove();
            generateReserve(response);
            socketGenerateReserve();
        })
        .fail(function (jqXHRm, textStatus, msg) {
            $('#loader').remove();
            alert(jqXHRm.responseJSON.message);
        });
}

function socketGenerateReserve() {
   var socket = io('http://api-reserveja-prod.herokuapp.com/');
   socket.on('connect', function() {
        console.log('Server Connected !');
        var params = {
            id: localStorage.getItem('_id')
        }
        socket.emit('join', params, function(err) {
            if (err) {
                alert(err);
              } else {}          
        });

        socket.on('teste', function(params) {
            console.log(params);
        });
   });
}

function generateReserve(response) {
    if(response.length > 0) {
        var template = $('#reserve_template').html();
        var rendered = Mustache.render(template, { reserves: response });
        $('#reserves').append(rendered);
    } else {
        var blank = "<tr>" +
        "<td></td>" +
        "<td></td>" +
        "<td><h6 style='font-size: 10pt;'>Nada de reservas no momento :(</h6></td>" +
        "<td></td>" +
        "<td></td>" +
        "</tr>";
        $('#reserves').append(blank);
    }
}

function saveReserveId(elem) {
    localStorage.setItem('_idReserve', $(elem).attr('data-id'));
    window.location.href = 'Detalhes_Reserva.html';
}

function loadReserve() {
    $.ajax({
        method: "GET",
        url: 'https://api-reserveja-prod.herokuapp.com/reserve/r' + localStorage.getItem('_idReserve'),
        headers: {
            "X-Auth": localStorage.getItem("oauth")
        },
        contentType: "application/json"
    })
        .done(function (response) {
            $('#loader').remove();
            var template = $('#reserve_template').html();
            var rendered = Mustache.render(template, { 
                username: response.username,
                numberOfPeople: response.numberOfPeople,
                hourEntrance: response.hourEntrance,
                dateEntrance: response.dateEntrance
             });
            $('#details_reserve').append(rendered);
        })
        .fail(function (jqXHRm, textStatus, msg) {
            $('#loader').remove();
            alert(jqXHRm.responseJSON.message);
        });
}